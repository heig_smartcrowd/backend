Smartcrowd backend
==================

The smartcrowd backend, providing the API used by the smartphones.

Config through environment variables
------------------------------------

Available variables :

  - REDIS_PORT=6379
  - REDIS_HOST=127.0.0.1
  - NODE_ENV='development'|'production'
  - PORT=80


REST API
--------

JSON spec is in src/schemas.js

HTTP return code
----------------
- 400 is returned if Malformed/Invalid JSON is sent
- 503 is returned if database is unavailable and request should be retried
- 500 is return if something unexpected happened

/readings (POST)
................
Insert new data in the database.
You can submit gzipped-JSON POST data to this url if the 'Content-Encoding'
header is set to 'gzip'.

See example_readings/readings1.json for a sample JSON data.

/snapshot (GET)
.......................
Query a 'snapshot' of the positions of all the users and staff members that
were active during the last 10 minutes.

Alternatively, you can use **/snapshot/true** to query for the positons of
all the users regardless of latest activity (since the beginning of time)

Example response ::

  [
    {
      "phone_id": "xitxzuazoi",
      "latitude": 46.77879238447419,
      "longitude": 6.658788634693431,
      "staff": false
    },
    {
      "phone_id": "kuhqocutgz",
      "latitude": 46.778842398097126,
      "longitude": 6.657594517897212,
      "staff": true
    },
  ]


Run
---
This is a nodejs app with a mongodb backend :

  npm install

  make mongo  # start the mongodb server

  make node  # start the nodejs server in development mode

  PORT=4350 make node-prod # start nodejs server in production mode

You can upload some example readings with :

  post_reading.sh example_readings/readings1.json

Test
----
Running the test requires mocha :

  npm install mocha -S -g

And then run with

  make test


JSON format
-----------

To POST a new list of readings to the app, perform a POST request
on /readings with a json object containing a list of readings ::

  {
    "phone_id" : string,
    "readings": [
      ...
      individual reading objects as described above
      ...
    ]
  }

For example ::

  {
    "phone_id" : "phone1",
    "readings" : [
      {
        "type" : "location",
        "phone_timestamp" : "1",
        "latitude" : 1.1,
        "longitude" : 1.2,
        "altitude" : 1.3
      },
      {
        "type" : "location",
        "phone_timestamp" : "2",
        "latitude" : 2.1,
        "longitude" : 2.2,
        "altitude" : 2.3
      }
    ]
  }

Format for various sensors
..........................
Location ::

  {
    "type" : <"location"|"accelerometer">,
    "phone_timestamp" : string (timestamp in UTC)
    "latitude" : float,
    "longitude" : float,
    "altitude" : float
  }

Accelerometer (work in progress, not clear what is the best way) ::

  {
    "type" : <"location"|"accelerometer">,
    "phone_timestamp" : string (timestamp in UTC)
    "count" : int
    "xdata" : array of floats,
    "ydata" : array of floats,
    "zdata" : array of floats
  }


References
----------
Some links about storing timeseries with mongodb :

  http://blog.mongodb.org/post/65517193370/schema-design-for-time-series-data-in-mongodb

  http://dba.stackexchange.com/questions/14533/how-should-i-store-time-series-in-mongodb

  http://www.slideshare.net/sky_jackson/time-series-data-storage-in-mongodb

  http://www.quora.com/Time-Series/What-is-the-best-way-to-store-time-series-data-in-MongoDB

It actually seems pretty horrible.... I don't want to have to split the
readings into one document each hour or something like that. And we cannot
store all the readings into one document per phone because the document size
is limited. The only option is to store each reading as a single document.

With Cassandra :

  http://planetcassandra.org/blog/post/getting-started-with-time-series-data-modeling/

