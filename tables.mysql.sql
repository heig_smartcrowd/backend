-- We use denormalized tables with no direct foreign key between the phone_id
-- of the various reading types and the main phones table. This is to
-- avoid having to check if the phone_id is in the database before every
-- insert.
USE smartcrowd;
-- USE smartcrowdtest;
CREATE TABLE IF NOT EXISTS phone_infos
(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  phone_id VARCHAR(128) NOT NULL,
  phone_timestamp DOUBLE,
  server_timestamp DOUBLE,
  -- TODO: Could use json, but it's not very well supported by drivers
  -- metadata json,
  infos TEXT
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS accelerometers
(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  phone_id VARCHAR(128) NOT NULL,
  x DOUBLE,
  y DOUBLE,
  z DOUBLE,
  phone_timestamp DOUBLE,
  server_timestamp DOUBLE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS batteries
(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  phone_id VARCHAR(128) NOT NULL,
  raw_level DOUBLE,
  scale DOUBLE,
  status integer,
  phone_timestamp DOUBLE,
  server_timestamp DOUBLE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS locations
(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  phone_id VARCHAR(128) NOT NULL,
  phone_timestamp DOUBLE,
  latitude DOUBLE,
  longitude DOUBLE,
  server_timestamp DOUBLE,
  altitude DOUBLE,
  provider TEXT
) ENGINE=InnoDB;

-- This is sort of a "backup" table that stores all the raw json readings
-- received. This is just in case
CREATE TABLE IF NOT EXISTS raw_json
(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  phone_id  VARCHAR(128) NOT NULL,
  server_timestamp DOUBLE,
  -- Could use postgres' json, but not well supported by drivers
  json TEXT
) ENGINE=InnoDB;
