CREATE DATABASE IF NOT EXISTS `smartcrowdtest`;
GRANT ALL on `smartcrowdtest`.* to 'smartcrowdtest'@'localhost' IDENTIFIED BY 'smartcrowdtest';
CREATE DATABASE IF NOT EXISTS `smartcrowd`;
GRANT ALL on `smartcrowd`.* to 'smartcrowd'@'localhost' IDENTIFIED BY 'smartcrowd';