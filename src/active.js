// Module to handle maintain a list of the N most recent active users with
// activity in the M last minutes
// This uses redis with the following structures :
//
// Each phone is associated with an expirable key that contains a stringified
// json. This key contains the latest location for the phone.
// For example, for phone with id f423409 and phone_timestamp=1234567
//    SET location.phone:f423409 "{\"lat\":-4,\"lng\":-5,\"staff\"}"
//    EXPIREAT location.phone:f423409 1234567
//
// We maintain a sorted set of the 100 most recent phones by using
//    ZADD active_phones 1234567 f423409
//    ZREMRANGEBYRANK active_phones 0 99
//
// With those two structures, to query the 100 most recent positions of the
// phone active in the last 10 minutes, we have to :
// - Get each phoneid from active_phones
// - Check if the associated key in location.phone:<phoneid> has expired
//
// In addition, some phones can be marked as staff. We also use redis for that
// (although it's not the most durable database, staff indicator is not really
// critical for now and redis makes it really easy).
// We simply add phoneid of staff members to the staff set and this is checked
// when saving the location
//
var redis = require('redis')
  , async = require('async')
  , error = require('./myerror');

var REDIS_PORT = process.env.REDIS_PORT || '6379';
var REDIS_HOST = process.env.REDIS_HOST || '127.0.0.1';
var REDIS_OPTIONS = {
  retry_max_delay: 30 * 1000, // ms
};
console.log('redis on : ' + REDIS_HOST + ':' + REDIS_PORT);

var client = redis.createClient(REDIS_PORT, REDIS_HOST, REDIS_OPTIONS);
// Need to explicitely catch error otherwise it will just crash
client.on('error', function(err) {
  error.logError('Redis connection error', err);
});

//client.on('reconnecting', function() {
  //console.log('Redis trying to reconnect');
//});

var ACTIVE_PHONES = 'active_phones';
var LOC_KEY_PREFIX = 'location.phone:';

var STAFF_SET = 'staff';

// Expiration in seconds
var PHONE_EXPIRATION_TIME = 600; // 10 minutes
// Keep 200 most recent positions
var TOP_N = 200;

exports.client = client;

exports.selectDatabase = function(dbnum, done) {
  client.select(dbnum, done);
}

exports.clearDb = function(done) {
  client.flushdb(done);
}

// For a given phoneid, update the location.phone list with the given
// data object (which will be stringified)
// The key is set to expire in expires_in_s seconds
exports.savePhoneLocation = function(phoneid, phone_timestamp, data, cb) {
  if (!client.connected) {
    cb(new Error('Redis not connected'));
    return;
  }
  var timestampS = Math.round(phone_timestamp/1000.0);
  var phone_loc_key = LOC_KEY_PREFIX + phoneid;
  function saveToRedis(data, cb) {
    client.multi()
      .set(phone_loc_key, JSON.stringify(data))
      .expireat(phone_loc_key, timestampS + PHONE_EXPIRATION_TIME)
      .zadd(ACTIVE_PHONES, timestampS, phone_loc_key)
      // See http://redis.io/commands/zremrangebyrank
      .zremrangebyrank(ACTIVE_PHONES, 0, -(TOP_N + 1))
      .exec(function(err, replies) {
        cb(err);
      });
  }
  client.sismember(STAFF_SET, phoneid, function onIsStaff(err, staff) {
    if (err) {
      cb(err);
      return;
    }
    data['staff'] = staff === 1;
    saveToRedis(data, cb);
  });
}

// done(err, phone_datas)
exports.getRecentActive = function(done) {
  if (!client.connected) {
    done(new Error('Redis not connected'), null);
    return;
  }

  function onPhoneId(phoneid, mapCb) {
    // Check if associated key has expired
    var phone_loc_key = phoneid;
    client.get(phone_loc_key, mapCb);
  }

  function filterExpired(phone_data, filterCb) {
    filterCb(phone_data != null);
  }

  client.zrevrange(ACTIVE_PHONES, 0, TOP_N, function(err, phones) {
    if (err) {
      done(err);
      return;
    }
    async.mapLimit(phones, 2, onPhoneId, function onMapResult(err, results) {
      if (err) {
        done(err);
        return;
      }
      async.filter(results, filterExpired, function(phone_datas) {
        var outdata = [];
        for (var i = 0; i < phone_datas.length; ++i) {
          outdata.push(JSON.parse(phone_datas[i]));
        }
        done(err, outdata);
      });
    });
  });
}
