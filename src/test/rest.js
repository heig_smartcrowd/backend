// Integration TestSuite for REST api. This requires (on localhost):
// - a running mongod, see Makefile
// - a running redis server (we use database 1 for tests)
var assert = require('assert')
  , http = require('http')
  , request = require('supertest')
  , app = require('../app').app
  , async = require('async')
  , mongoManager = require('../mongo_manager')
  , testdata = require('./sample_data')
  , _ = require('underscore')
  , zlib = require('zlib')
  , active_phones = require('../active');


var MONGO_TEST_PORT = 27018;
var TEST_PORT = 8910;
var TEST_URL = 'http://localhost:' + TEST_PORT;

var REDIS_TEST_DB = 1;

var test_mongo_string = 'mongodb://localhost:' + MONGO_TEST_PORT + '/testing';
app.set('mongodb_connection_string', test_mongo_string);
app.set('port', TEST_PORT);

function emptyReadings(done) {
  // Empty "readings" collection
  mongoManager.execute(function onDb(err, db) {
    assert.equal(err, null);
    db.collection("readings", function removeAll(err, collection) {
      assert.equal(err, null);
      collection.remove(function(err){
        assert.equal(err, null);
        done();
      });
    });
  });
}

// select REDIS_TEST_DB and empty
function setupRedis(done) {
  async.series([
    function (done) {
      active_phones.selectDatabase(REDIS_TEST_DB, done);
    },
    active_phones.clearDb],
    done);
}

describe('server', function() {
  var server = null;

  before(function startServer(done){
    app.listen(function(srv) {
      server = srv;
      done();
    });
  });
  before(setupRedis);

  after(function stopServer(done) {
    server.close();
    done();
  });

  describe('/readings', function() {
    beforeEach(emptyReadings);

    function checkInDb(res) {
      // TODO: This is a bit ugly, but guarantee a deep copy
      var expected = JSON.parse(JSON.stringify(testdata.READINGS1_UNTRANSPOSED));
      // The in-db readings will have an additional phone_id
      expected["readings"] = _.map(expected["readings"], function(r) {
        r["phone_id"] = "myphone";
        return r;
      });

      mongoManager.execute(function onDb(err, db) {
        assert.equal(err, null);
        db.collection('readings', function onColl(err, collection) {
          assert.equal(err, null);
          collection.find()
                    .sort({'phone_timestamp':1})
                    .fields({'_id':0, 'server_timestamp':0})
                    .toArray(function onResult(err, items) {
            assert.equal(err, null);
            assert.deepEqual(items, expected["readings"]);
          });
        });
      });
    }

    function checkInRedis(res) {
      active_phones.client.zrevrange('active_phones', 0, 100,
        function(err, results) {
          assert.equal(err, null);
          assert.equal(results.length, 1);
          assert.equal(results[0], "location.phone:myphone");
      });
    }

    it('should accept READINGS1', function(done) {
      request(app)
        .post('/readings')
        .send(testdata.READINGS1)
        .expect(200)
        .expect(checkInDb)
        .expect(checkInRedis)
        .end(done);
    });

    it('should accept gzipped READINGS1', function(done) {
      zlib.gzip(JSON.stringify(testdata.READINGS1), function(err, buffer) {
        assert.equal(err, null);
        var ra =request(app)
          .post('/readings')
          .set('Content-Encoding', 'gzip')
          .set('Content-Type', 'application/json');
        ra.write(buffer);
        ra.expect(200)
        ra.expect(checkInDb)
        ra.expect(checkInRedis)
        ra.end(done);
      });
    });

    it('should return 400 on malformed data', function(done) {
      var malformed = '{ "phone_id" : "myphone';
      request(app)
        .post('/readings')
        .set('Content-Type', 'application/json')
        .send(malformed)
        .expect(400)
        .end(done);
    });

    it('should return 400 on well-formed by invalid json', function(done) {
      var invalid = { "phone_id" : "blah" };
      request(app)
        .post('/readings')
        .set('Content-Type', 'application/json')
        .send(invalid)
        .expect(400)
        .end(done);
    });
  });

  describe('/snapshot', function() {
    var now = new Date().getTime();
    var readingsData = [
      {
        "phone_id" : "phone1",
        "type" : "location",
        "phone_timestamp" : now - 5,
        "latitude" : 1.1,
        "longitude" : 1.2,
        "altitude" : 1.3
      },
      {
        "phone_id" : "phone1",
        "type" : "location",
        "phone_timestamp" : now,
        "latitude" : 2.1,
        "longitude" : 2.2,
        "altitude" : 2.3
      },
      {
        "phone_id" : "phone2",
        "type" : "location",
        "phone_timestamp" : now + 2,
        "latitude" : 3.1,
        "longitude" : 3.2,
        "altitude" : 3.3,
      },
      {
        "phone_id" : "phone3",
        "type" : "location",
        "phone_timestamp" : now - 200*60*1000,
        "latitude" : 4.1,
        "longitude" : 4.2,
        "altitude" : 4.3,
      }
    ];

    before(setupRedis);
    // Mark phone2 as staff
    before(function (done) {
      active_phones.client.sadd('staff', 'phone2', done);
    });

    before(function (done) {
      function save(data, cb) {
        var rdata = {
          // For testing, add phone_id. Makes it easier to check
          "phone_id" : data['phone_id'],
          "latitude" : data["latitude"],
          "longitude" : data["longitude"]
        };
        active_phones.savePhoneLocation(data['phone_id'],
                                        data['phone_timestamp'],
                                        rdata,
                                        cb);
      }
      async.map(readingsData, save, done);
    });

    // Helper for checkResults
    function findPhone(res, phone_id) {
      for (var i = 0; i < res.body.length; ++i) {
        if (res.body[i]["phone_id"] === phone_id) {
          return res.body[i];
        }
      }
      assert(false, "Couldn't find phone id : " + phone_id);
    }

    function checkResults(res) {
      assert.equal(res.body.length, 2);

      var phone1 = findPhone(res, "phone1")
        , phone2 = findPhone(res, "phone2");

      assert.deepEqual(phone1, {phone_id:'phone1',
                                latitude: 2.1,
                                longitude: 2.2,
                                staff:false});
      assert.deepEqual(phone2, {phone_id:'phone2',
                                latitude: 3.1,
                                longitude: 3.2,
                                staff:true});
    }

    it('should work with /', function(done) {
      request(app)
        .get('/snapshot')
        .expect(200)
        .expect(checkResults)
        .end(done);
    });
  });
});
