// Readings, ordered in the order of timestamps (this is just to simplify
// testing, but is not a requirement)
var READINGS1 =
{
  "phone_id": "myphone",
  "readings": [
    {
      "type": "battery",
      "count": 2,
      "phone_timestamp": [1, 2],
      "raw_level": [92, 93],
      "scale": [100, 101],
      "status": [2, 3]
    },
    {
      "type": "location",
      "count": 2,
      "phone_timestamp": [3, 4],
      "accuracy": [14.142, 15],
      "altitude": [0, 5],
      "latitude": [46.5291307, 49],
      "longitude": [6.6303329, 7],
      "provider": ["p1", "p2"],
      "speed": [0, 1]
    },
    {
      "type": "bluetooth",
      "count": 2,
      "phone_timestamp": [5, 6],
      "devices": [
        { "rssi": [-90, -56], "address": ["a1", "a2"], "name": ["n1", "n2"] },
        { "rssi": [-70], "address": ["a1"], "name": ["n1"] }
      ]
    },
    {
      "type": "accelerometer",
      "count": 2,
      "phone_timestamp": [7, 8],
      "x": [0.22185141, -5.607636],
      "y": [-4.7949696, 1.115056],
      "z": [-0.6710205, 2.2289257]
    },
    {
      "type": "phone_info",
      "phone_timestamp": 9,
      "android_version": "4.4.2",
      "count": 1,
      "manufacturer": "LGE",
      "model": "Nexus 4"
    },
    {
      "type": "sensor_info",
      "phone_timestamp": 10,
      "count": 3,
      "min_delay": [5000, 20000, 5000],
      "name": ["LGE Accelerometer Sensor", "LGE Magnetometer Sensor", "Orientation"],
      "power": [0.5, 5, 9.1],
      "resolution": [0.0011901855, 0.14953613, 0.1],
      "sensor_type": [1, 2, 3]
    }
  ]
};

var READINGS1_UNTRANSPOSED = {
  "phone_id" : "myphone",
  "readings": [
    {
      "type": "battery",
      "phone_timestamp" : 1,
      "raw_level" : 92,
      "scale": 100,
      "status": 2
    },
    {
      "type": "battery",
      "phone_timestamp" : 2,
      "raw_level" : 93,
      "scale": 101,
      "status": 3
    },
    {
      "type": "location",
      "phone_timestamp" : 3,
      "accuracy" : 14.142,
      "altitude" : 0,
      "latitude" : 46.5291307,
      "longitude" : 6.6303329,
      "provider" : "p1",
      "speed" : 0
    },
    {
      "type": "location",
      "phone_timestamp": 4,
      "accuracy" : 15,
      "altitude": 5,
      "latitude" : 49,
      "longitude": 7,
      "provider": "p2",
      "speed" : 1
    },
    {
      "type": "bluetooth",
      "phone_timestamp": 5,
      "devices": {
        "rssi": [-90, -56],
        "address": ["a1", "a2"],
        "name" : ["n1", "n2"]
      }
    },
    {
      "type": "bluetooth",
      "phone_timestamp": 6,
      "devices": {
        "rssi": [-70],
        "address": ["a1"],
        "name" : ["n1"]
      }
    },
    {
      "type": "accelerometer",
      "phone_timestamp": 7,
      "x" : 0.22185141,
      "y" : -4.7949696,
      "z" : -0.6710205
    },
    {
      "type": "accelerometer",
      "phone_timestamp": 8,
      "x" : -5.607636,
      "y" : 1.115056,
      "z" : 2.2289257
    },
    {
      "type": "phone_info",
      "phone_timestamp": 9,
      "android_version": "4.4.2",
      "manufacturer": "LGE",
      "model": "Nexus 4"
    },
    {
      "type": "sensor_info",
      "phone_timestamp": 10,
      "min_delay": [5000, 20000, 5000],
      "name": ["LGE Accelerometer Sensor", "LGE Magnetometer Sensor", "Orientation"],
      "power": [0.5, 5, 9.1],
      "resolution": [0.0011901855, 0.14953613, 0.1],
      "sensor_type": [1, 2, 3]
    }
  ]
};

module.exports = {
  READINGS1 : READINGS1,
  READINGS1_UNTRANSPOSED : READINGS1_UNTRANSPOSED
};
