var assert = require('assert');
var testdata = require('./sample_data');
var schemas = require('../schemas');
var async = require('async');
var testdata2 = require('./sample_data2');

function assertValidates(obj, schema) {
  result = schemas.validate(obj, schema);
  assert(result['valid'], "error : " + result['error']
    + ", missing : " + result['missing']);
}

function assertRejects(obj, schema) {
  result = schemas.validate(obj, schema);
  assert(!result['valid'], "Should reject");
}

describe('schema', function() {
  describe('READINGS1', function() {
    // -- Validation tests
    it("battery", function() {
      assertValidates(testdata.READINGS1["readings"][0], schemas.BATTERY_SCHEMA);
    });
    it("location", function() {
      assertValidates(testdata.READINGS1["readings"][1], schemas.LOCATION_SCHEMA);
    });
    it("bluetooth", function() {
      assertValidates(testdata.READINGS1["readings"][2], schemas.BLUETOOTH_SCHEMA);
    });
    it("accelerometer", function() {
      assertValidates(testdata.READINGS1["readings"][3], schemas.ACCELEROMETER_SCHEMA);
    });
    it("phone_info", function() {
      assertValidates(testdata.READINGS1["readings"][4], schemas.PHONE_INFO_SCHEMA);
    });
    it("sensor_info", function() {
      assertValidates(testdata.READINGS1["readings"][5], schemas.SENSOR_INFO_SCHEMA);
    });
    it("validates whole readings bundle", function() {
      assertValidates(testdata.READINGS1, schemas.READINGS_BUNDLE_SCHEMA);
    });

    // -- Some rejections test
    it("rejects battery as bluetooth", function() {
      assertRejects(testdata.READINGS1["readings"][0], schemas.BLUETOOTH_SCHEMA);
    });
    it("rejects bluetooth as battery", function() {
      assertRejects(testdata.READINGS1["readings"][2], schemas.BATTERY_SCHEMA);
    });
    it("rejects accelerometer as phone_info", function() {
      assertRejects(testdata.READINGS1["readings"][3], schemas.PHONE_INFO_SCHEMA);
    });
    it("rejects phone_info as accelerometer", function() {
      assertRejects(testdata.READINGS1["readings"][4], schemas.ACCELEROMETER_SCHEMA);
    });
  });
  describe('READINGS2', function() {
    it("battery", function() {
      assertValidates(testdata2.READINGS2["readings"][0],
                      schemas.BATTERY_SCHEMA);
    });
    it("location", function() {
      assertValidates(testdata2.READINGS2["readings"][1],
                      schemas.LOCATION_SCHEMA);
    });
    it("bluetooth", function() {
      assertValidates(testdata2.READINGS2["readings"][2],
                      schemas.BLUETOOTH_SCHEMA);
    });
    it("accelerometer", function() {
      assertValidates(testdata2.READINGS2["readings"][3],
                      schemas.ACCELEROMETER_SCHEMA);
    });

    it("validates whole readings bundle", function() {
      assertValidates(testdata2.READINGS2, schemas.READINGS_BUNDLE_SCHEMA);
    });
  });
});

describe('untranspose', function() {
  describe('READINGS1', function() {
    // Check that readings[sourceIdx] in READINGS1 gets untransposed to
    // entries expectedSlice[0] to expectedSlice[1] in READINGS1_UNTRANSPOSED
    function checkUntranspose(sourceIdx, expectedSlice) {
      var expected = testdata.READINGS1_UNTRANSPOSED["readings"]
                     .slice(expectedSlice[0], expectedSlice[1]);
      var r = schemas.untransposeReading(
                     testdata.READINGS1["readings"][sourceIdx]);
      for (var i = 0; i < expected.length; ++i) {
        assert.deepEqual(r[i], expected[i]);
      }
    }
    it("untransposes battery", function() {
      checkUntranspose(0, [0, 2]);
    });
    it("untransposes location", function() {
      checkUntranspose(1, [2, 4]);
    });
    it("untransposes bluetooth", function() {
      checkUntranspose(2, [4, 6]);
    });
    it("untransposes accelerometer", function() {
      checkUntranspose(3, [6, 8]);
    });
    it("untransposes phone_info", function() {
      checkUntranspose(4, [8, 9]);
    });
    it("untransposes phone_sensor_info", function() {
      checkUntranspose(5, [9, 10]);
    });
  });
});

describe('untransposeBundle', function() {
  describe('READINGS1', function() {
    it("untranspose whole bundle", function() {
      var r = schemas.untransposeBundle(testdata.READINGS1);
      assert.deepEqual(r, testdata.READINGS1_UNTRANSPOSED);
    });
  });
});
