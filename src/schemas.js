var tv4 = require('tv4');
var _ = require('underscore');

var numberArray = {
  "type": "array",
  "items" : { "type": "number" },
};

var stringArray = {
  "type": "array",
  "items" : { "type": "string" },
};

var countType = {
  "type" : "integer",
  "minimum" : 0
}

// Schemas for JSON validation
var LOCATION_SCHEMA = {
  "title" : "Location",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["location"] },
    "count" : countType,
    "phone_timestamp": numberArray,
    "accuracy" : numberArray,
    "latitude" : numberArray,
    "longitude" : numberArray,
    "altitude" : numberArray,
    "provider" : stringArray,
    "speed" : numberArray
  },
  "required" : ["type", "count", "phone_timestamp", "latitude", "longitude",
                "accuracy", "altitude", "provider", "speed"],
  "additionalProperties": false
};

var ACCELEROMETER_SCHEMA = {
  "title" : "Accelerometer",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["accelerometer"] },
    "count" : countType,
    "phone_timestamp": numberArray,
    "x" : numberArray,
    "y" : numberArray,
    "z" : numberArray
  },
  "required" : ["type", "count", "phone_timestamp", "x", "y", "z"],
  "additionalProperties": false
};

var BATTERY_SCHEMA = {
  "title" : "Battery",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["battery"] },
    "count" : countType,
    "phone_timestamp": numberArray,
    "raw_level" : numberArray,
    "scale" : numberArray,
    "status" : numberArray,
  },
  "required" : ["type", "count", "phone_timestamp", "raw_level", "scale", "status"],
  "additionalProperties": false
};

var BLUETOOTH_SCHEMA = {
  "title" : "Battery",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["bluetooth"] },
    "count" : countType,
    "phone_timestamp": numberArray,
    "devices": {
      "type": "array",
      "items": {
        "type": "object",
        "additionalProperties" : true
      }
    }
  },
  "required" : ["type", "count", "phone_timestamp", "devices"],
  "additionalProperties": false
};

var PHONE_INFO_SCHEMA = {
  "title" : "PhoneInfo",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["phone_info"] },
    "phone_timestamp": { "type": "number" },
    "bluetooth_address" : { "type" : "string" }
  },
  "required" : ["type", "phone_timestamp"],
  "additionalProperties": true
};

var SENSOR_INFO_SCHEMA = {
  "title" : "SensorInfo",
  "type" : "object",
  "properties": {
    "type" : { "enum": ["sensor_info"] },
    "count" : countType,
    "phone_timestamp" : { "type" : "number" },
    "min_delay" : numberArray,
    "name" : stringArray,
    "power" : numberArray,
    "resolution" : numberArray,
    "sensor_type" : numberArray
  },
  "required": ["type", "phone_timestamp", "count", "min_delay", "name", "power",
               "resolution", "sensor_type"],
  "additionalProperties": false
};

var READINGS_BUNDLE_SCHEMA = {
  "title" : "ReadingsBundle",
  "type": "object",
  "properties": {
    "phone_id" : { "type" : "string" },
    "readings": {
      "type": "array",
      "minItems" : 1,
      "items": {
        "type": "object",
        "oneOf": [
          { "$ref" : '#/definitions/locationSchema' },
          { "$ref" : '#/definitions/bluetoothSchema' },
          { "$ref" : '#/definitions/accelerometerSchema' },
          { "$ref" : '#/definitions/batterySchema' },
          { "$ref" : '#/definitions/phoneInfoSchema' },
          { "$ref" : '#/definitions/sensorInfoSchema' }
        ]
      }
    }
  },
  "required": ["phone_id", "readings"],
  "definitions": {
    "locationSchema": LOCATION_SCHEMA,
    "accelerometerSchema": ACCELEROMETER_SCHEMA,
    "batterySchema": BATTERY_SCHEMA,
    "bluetoothSchema": BLUETOOTH_SCHEMA,
    "phoneInfoSchema": PHONE_INFO_SCHEMA,
    "sensorInfoSchema" : SENSOR_INFO_SCHEMA
  }
};

function validate(obj, schema) {
  return tv4.validateResult(obj, schema);
}

function validateReadings(obj) {
  return validate(obj, READINGS_BUNDLE_SCHEMA);
}

// Given a dict in transposed format (where only fields in transposedFields
// are transposed), returns a list of untransposed readings
// It is assumed the transposed object as a "count" property
function untranspose(tobj, transposedFields) {
  var out = [];
  var count = tobj["count"];
  // Otherwise, we might end up duplicating non-transposed readings
  // (phone_info, sensor_info) with a count property
  if (transposedFields.length == 0) {
    count = 1;
  }
  var commonFields = _.difference(_.keys(tobj), transposedFields);
  for (var i = 0; i < count; ++i) {
    d = {}
    _.each(commonFields, function(f) {
      if (f == "count") {
        return;
      }
      d[f] = tobj[f];
    });
    _.each(transposedFields, function(f) { d[f] = tobj[f][i]; });
    out.push(d);
  }
  return out;
}

// For each reading type, define which fields are received transposed
var READINGS_TRANSPOSED_FIELDS = {
  "battery": ["phone_timestamp", "raw_level", "scale", "status"],
  "location": ["phone_timestamp", "accuracy", "altitude", "latitude", "longitude",
               "provider", "speed"],
  "bluetooth": ["phone_timestamp", "devices"],
  "accelerometer" : ["phone_timestamp", "x", "y", "z"],
  "phone_info" : [],
  "sensor_info" : []
};

function untransposeReading(reading) {
  return untranspose(reading, READINGS_TRANSPOSED_FIELDS[reading["type"]]);
}

function untransposeBundle(bundle) {
  var out = {};
  out["phone_id"] = bundle["phone_id"];
  var readings = [];
  for (var i = 0; i < bundle.readings.length; ++i) {
    var rlist = untransposeReading(bundle.readings[i]);
    _.each(rlist, function(r) {
      readings.push(r);
    });
  }
  out["readings"] = readings;
  return out;
}

module.exports = {
  READINGS_BUNDLE_SCHEMA : READINGS_BUNDLE_SCHEMA,
  LOCATION_SCHEMA : LOCATION_SCHEMA,
  ACCELEROMETER_SCHEMA : ACCELEROMETER_SCHEMA,
  BATTERY_SCHEMA : BATTERY_SCHEMA,
  BLUETOOTH_SCHEMA : BLUETOOTH_SCHEMA,
  PHONE_INFO_SCHEMA : PHONE_INFO_SCHEMA,
  SENSOR_INFO_SCHEMA : SENSOR_INFO_SCHEMA,
  validateReadings : validateReadings,
  validate : validate,
  untranspose : untranspose,
  untransposeReading : untransposeReading,
  untransposeBundle: untransposeBundle
}
