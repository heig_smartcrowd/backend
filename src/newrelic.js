/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name : ['SmartCrowd'],
  /**
   * Your New Relic license key.
   */
  license_key : 'aba5bda18561a7b531ad7e2d15b87e1db1c01373',
  // Enable by settings environment variable NEW_RELIC_ENABLED=false
  agent_enabled: false,
  logging : {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level : 'info'
  }
};
