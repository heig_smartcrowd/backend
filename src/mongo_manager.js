// The official MongoClient doc recommend calling MongoClient.connect once
// and starting the http server only once the DB connection has been
// established. auto_reconnect should handle automatic reconnection.
//
// This works fine 99% of the time. But imagine the following situation :
//   1. Mongodb is running, the app is started. Everything is fine.
//   2. Suddenly mongodb crash.
//   3. A client makes a query.
//   4. Mongodb is restarted
//
//  With auto_reconnect set to true, the query from the client in (3) will
//  hang until the application reconnects to mongodb. This is BAD because we
//  don't want the client to wait, we want it to receive a 500 and retry at
//  a later tme.
//
//  With auto_reconnect set to false, the server returns 500 to the client in
//  (3). But when mongodb is restarted later, it doesn't reconnect
//  automatically.
//
//  What is needed to solve this situation is a wrapper that calls MongoClient
//  connect and automatically reconnects when a close event is fired.
//
// Based on https://github.com/hectorcorrea/mongoConnect/blob/master/index.js
// Interesting resources
//   http://blog.mongolab.com/2013/10/do-you-want-a-timeout/
var error = require('./myerror');
var MongoClient = require('mongodb').MongoClient;
var isConnected = false;
var dbUrl = null;
var db = null;
var dbOptions = {
  db: {},
  server: {
    connectTimeoutMS: 500,
    // Without auto_reconnect, if we have an inactivity period, we'll loose
    // the connection without getting notified about it (even if both node
    // and mongodb are running)
    // https://www.openshift.com/forums/openshift/mongodb-connections-keep-dropping
    //
    // You can reproducer by setting auto_reconnect to false, wait until
    // mongodb shows "end connection ... " messages. Then, perform any request
    // and you'll get a 500 even if mongo is running
    auto_reconnect: true,
    socketOptions:{ keepAlive: 1}
  },
  replSet: {},
  mongos: {}
};
var dbSetupCallback = null;
// Indicator variable to indicate a dbSetupCallback call is in progress
var dbSetupCalled = false;

var connectAndExecute = function(callback) {
  MongoClient.connect(dbUrl, dbOptions, function(err, dbConn) {
    if (err) {
      isConnected = false;
      callback(err);
      return;
    }

    isConnected = true;
    db = dbConn;
    db.on("open", function() {
      console.log('Reopened connection to mongodb');
      isConnected = true;
    });
    db.on("close", function() {
      console.log('Lost connection to mongodb');
      isConnected = false;
    });

    if (dbSetupCallback) {
      // Avoid calling it multiple times if we get multiple requests before
      // the setup has finished
      if (!dbSetupCalled) {
        console.log('Calling dbSetupCallback');
        dbSetupCalled = true;
        dbSetupCallback(db, function(success) {
          if (success) {
            dbSetupCallback = null;
          }
          dbSetupCalled = false;
        });
      }
    }

    callback(null, db);
  });
}

exports.execute = function(callback) {
  if (isConnected) {
    callback(null, db);
  } else {
    connectAndExecute(callback);
  }
}

// Setups mongodb
// - mongoUrl : the mongodb url to connect to
// - mongoOptions : options for the connection
// - setupcb : a function(db, done) where done is function(bool) that is called
// when a connection is successfully established. It is not called anymore
// once it has called done(true)
exports.setup = function(mongoUrl, mongoOptions, setupcb, verbose) {
  if (dbUrl != null) {
    console.log('MongoManager setup called twice');
    return;
  }
  dbUrl = mongoUrl;
  if (mongoOptions) {
    dbOptions = mongoOptions;
  }

  if (setupcb) {
    dbSetupCallback = setupcb;
  }
}

exports.isConnected = function() {
  return isConnected;
}
