// Error handling helpers
// https://groups.google.com/forum/#!topic/nodejs/WyBIaN4vVSI
//var newrelic = require('newrelic');
var rollbar = require('rollbar');

var errorsCount = {};

function addError(tag) {
  if (tag in errorsCount) {
    errorsCount[tag]++;
  } else {
    errorsCount[tag] = 1;
  }
}

function getErrorsCount() {
  return errorsCount;
}

// A wrapper function that returns a 500 HTTP response in case of error
//
// Usage :
// db.execute("my query", error.wrapError(res, 500, function(results) { ... }));
// wrapError(res, [statusCode], winCb)
// - res is the response
// - statusCode is the status code to send in case of error
// - winCb is the callback called if no error
function wrapError(response, statusCode, win) {
  return function(error, result) {
    if (error) {
      sendError(response, statusCode, "Internal " + statusCode, error);
    } else {
      // Pass the remaining arguments (minus error) to the win function
      win.apply(this, Array.prototype.slice.call(arguments, 1));
    }
  }
}

function logError(msg, error) {
  addError(msg);
  console.log("[" + new Date().toString() + "] error : " + msg, error);
  rollbar.reportMessage({'error':error, 'msg':msg});
  //newrelic.noticeError({'error':error, 'msg':msg});
}

function sendError(response, statusCode, msg, error) {
  response.writeHead(statusCode);
  response.end(msg);
  logError(msg, error);
}

module.exports = {
  wrapError: wrapError,
  sendError: sendError,
  logError: logError,
  getErrorsCount: getErrorsCount
};
