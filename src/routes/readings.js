var error = require('../myerror');
var async = require('async');
var humanize = require('humanize');
var crypto = require('crypto');

var schemas = require('../schemas');
var mongoManager = require('../mongo_manager');
var active_phones = require('../active');

function createIndex(collection, idx, options, cb) {
  collection.ensureIndex(idx, options,
    function(err, indexname) {
      if (err) {
        throw new Error('Error in ensureIndex', err.name, err.message);
      }
      cb();
  });
}

exports.onDBConnected = function(db, done) {
  db.collection('raw_json', function(err, collection) {
    if (err) {
      done(false);
      return;
    }
  });

  db.collection('readings', function(err, collection) {
    if (err) {
      done(false);
      return;
    }
    async.series([
        function(cb) {
          createIndex(collection,
            {'phone_id' : 1},
            null, cb);
        },
        function(cb) {
          createIndex(collection,
            {'phone_id' : 1,
             'phone_timestamp' : -1},
            null, cb);
        },
        function(cb) {
          createIndex(collection,
            {'type' : 1},
            null, cb);
        }],
      function(err, results) {
        if (err) {
          done(false);
        } else {
          done(true);
        }
      }
    );
  });
}

exports.snapshot = function(req, res) {
  active_phones.getRecentActive(error.wrapError(res, 503, function (results) {
    res.send(results);
  }));
}

// Return a dict indicating the number of elements in each collection
// {
//  "table1" : 2,
//  "table2" : 399
// }
//
// cb should be function(err, countmap)
function getPerTypeCounts(cb) {
  function onError(err) {
    error.logError("getPerTypeCounts.DB", err);
    cb(null, {'error' : 'DB unavailable'});
  }

  mongoManager.execute(function(err, db) {
    if (err) {
      onError(err);
      return;
    }
    db.collection('readings', function(err, collection) {
      if (err) {
        onError(err);
      } else {
        collection.count(function(err, count) {
          if(err) {
            onError(err);
          } else {
            cb(null, {'readings' : count});
          }
        });
      }
    })
  });
}

// Fetch the last received readings update from rawjson
// cb is function(err, lastReceived)
// Note that in case of db error, an JSON error message is returned (stats
// should continue to work in case of errors)
function getLastReceived(cb) {
  function onError(err) {
    error.logError("getLastReceived.DB", err);
    cb(null, {'error' : 'DB unavailable'});
  }

  mongoManager.execute(function onDb(err, db) {
    if (err) {
      onError(err);
      return;
    }
    db.collection('readings', function onColl(err, collection) {
      if (err) {
        onError(err);
      } else {
        // mongodb id contains creation date
        collection.find()
                  .sort({'server_timestamp':-1})
                  .limit(1)
                  .toArray(function onResult(err, doc) {
          if (err) {
            onError(err);
          } else {
            if (doc.length > 0) {
              cb(null, doc[0]);
            } else {
              cb(null, {});
            }
          }
        });
      }
    })
  });
}

exports.stats = function(req, res) {
  async.parallelLimit({
      "counts" : getPerTypeCounts,
      "errors_count" : function(cb) { cb(null, error.getErrorsCount()); },
      "last_received" : getLastReceived,
      "db_connected" : function(cb) { cb(null, mongoManager.isConnected()); },
    },
    2,
    error.wrapError(res, 500, function(results) {
      res.send(results);
    })
  );
}

// Simple healthcheck that tests db connectivity
exports.health = function(req, res) {
  mongoManager.execute(error.wrapError(res, 503, function onDB(db) {
    db.collection('readings', error.wrapError(res, 503, function onColl(collection) {
      res.send({'alive':'ok'});
    }));
  }));
}

exports.addReading = function(req, res) {
  // Insert into 'raw_json' collection. Note that this is non-blocking and
  // errors don't have any effect on the response. Also, we do not enforce
  // mongodb safe write. This is really just for backup
  mongoManager.execute(error.wrapError(res, 503, function onDB(db) {
    db.collection('raw_json', function(err, collection) {
      if (err) {
        error.logError("Accessing raw_json", err);
        return;
      }
      collection.insert(req.body, function(err, result) {
        if (err) {
          error.logError('Insert into raw_json', err);
        }
      });
    });
  }));

  // Validate incoming JSON
  var result = schemas.validateReadings(req.body);
  if (!result["valid"]) {
    error.sendError(res, 400, "Invalid JSON", {"data": req.body,
      "message": result['message'], "missing": result["missing"]})
    return;
  }
  var bundle = schemas.untransposeBundle(req.body);
  var phone_id = bundle.phone_id;
  var readings = bundle.readings;
  var server_timestamp = new Date().getTime();
  // Process readings :
  // - Add phone_id and server_timestamp for mongodb inserts
  // - Find (if any) the most recent location readings for redis insert
  var inserts = [];
  var most_recent_loc = null;
  var most_recent_loc_timestamp = -1;
  for (var i = 0; i < readings.length; i++) {
    var reading = readings[i];
    reading['phone_id'] = phone_id;
    reading['server_timestamp'] = server_timestamp;
    //console.log('Adding reading : ' + JSON.stringify(reading));
    inserts.push(reading);

    if (reading['type'] == 'location') {
      if (most_recent_loc == null
          || reading['phone_timestamp'] > most_recent_loc_timestamp) {
        //var shasum = crypto.createHash('sha1');
        //var obf_phone_id = shasum.update(phone_id).digest('hex');
        most_recent_loc = {
          'phone_id' : phone_id,
          'latitude' : reading['latitude'],
          'longitude' : reading['longitude']
        };
        most_recent_loc_timestamp = reading['phone_timestamp'];
      }
    }
  }

  // Insert into redis
  if (most_recent_loc) {
    active_phones.savePhoneLocation(phone_id, most_recent_loc_timestamp,
                                    most_recent_loc, function(err) {
      if (err) {
        error.logError('savePhoneLocation', err);
      }
    });
  }

  if (inserts.length == 0) {
    console.log('Empty inserts...');
    res.send(200);
    return;
  }

  // Insert into mongo
  // TODO: Should we insert in different collections based on type ?
  mongoManager.execute(error.wrapError(res, 503, function onDB(db) {
    db.collection('readings', error.wrapError(res, 503, function onColl(collection) {
      collection.insert(inserts, {safe:true},
        error.wrapError(res, 500, function onInsert(result) {
        //console.log('Success : ' + JSON.stringify(result[0]));
        res.send(200);
      }));
    }))
  }));
}

