// Main application.
// Use it with app.listen()
var express = require('express');
var morgan = require('morgan');
var compress = require('compression');

var mongoManager = require('./mongo_manager');
var readings = require('./routes/readings');
var error = require('./myerror');
var bodyParser = require('./custom_body_parser');
var rollbar = require('rollbar');

rollbar.init("b5e401ce273f4384a9508d8851f6d47a", {
  environment : process.env.NODE_ENV || 'development'
});

var app = express();

//app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(compress({ threshold: 0}))

var env = process.env.NODE_ENV || 'development';
if (env == 'development') {
  console.log('Using development environment');
  app.use(morgan('dev'));
  app.set('port', 4321);
}
if (env == 'production') {
  console.log('Using production environment');
  app.use(morgan());
  console.log('$PORT = ' + process.env.PORT);
  if (! process.env.NEW_RELIC_APP_NAME) {
    throw('Environment variable NEW_RELIC_APP_NAME must be defined for prod');
  }
  app.set('port', process.env.PORT || 4321);
  // Recommended by express doc
  // http://expressjs.com/guide.html#proxies
  app.enable('trust proxy')
}

// Custom error handler
app.use(function(err, req, res, next) {
  if (!err) {
    return next();
  }
  var statusCode = 500;
  var msg = 'Unknown error';
  // This means we got malformed JSON
  if (err instanceof SyntaxError) {
    statusCode = 400;
    msg = 'Syntax error';

  }
  error.sendError(res, statusCode, msg, err);
});

// POST a new reading
app.post('/readings', readings.addReading);

// Return a snapshot of the most recent active phones
app.get('/snapshot', readings.snapshot);

app.get('/stats', readings.stats);

// A healthcheck that will return {'alive':'ok'} and status 200 if web server 
// (obviously) AND DB connection are ok
app.get('/health', readings.health);

app.set('mongodb_connection_string',
        'mongodb://localhost:27017/smartcrowd?journal=true')

// Override the original expressjs app.listen to setup DB before starting
// the http server
app.real_listen = app.listen;

// cb(server) is a callback that will receive the listening http server once
// it has been started
app.listen = function(cb) {
  var connString = app.get('mongodb_connection_string');
  var server = null;

  // See mongo_manager.js : dbOptions for connection options
  mongoManager.setup(connString, null, readings.onDBConnected);

  // Start application
  var port = app.get('port');
  console.log("Listening on port " + port);
  var server = app.real_listen(port);
  if (cb) {
    cb(server);
  }
}

exports.app = app;
