all:
	# TODO: Can we get a list of target from some variable ?
	@echo 'Specify target : mongo, node, node-prod test'

.PHONY: mongo
mongo:
	mkdir -p mongo_data/prod && \
  mongod --dbpath ./mongo_data/prod

.PHONY: node
node:
	NODE_ENV=development node-supervisor src/server.js

.PHONY: node-prod
node-prod:
	NODE_ENV=production node src/server.js

.PHONY: test
test:
	@echo 'Cleaning mongodb'
	@mkdir -p mongo_data/testing
	@rm -rf mongo_data/testing/*
	mongod --dbpath ./mongo_data/testing --port 27018 2>&1 1>.mongotest.log & echo "$$!" > .mongotest.pid
	@sleep 1
	@# - is used to ignore errors
	-NEW_RELIC_NO_CONFIG_FILE=1 NEW_RELIC_ENABLED=0 mocha -t 25000 --reporter spec src/test
	@echo 'Terminating mongodb'
	@kill -9 `cat .mongotest.pid`

